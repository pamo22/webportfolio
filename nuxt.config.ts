// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	modules: [
		'@nuxtjs/tailwindcss'
	],
  app: {
    head: {},
  },
  devtools: { enabled: true },
  css: ['~/assets/css/main.css'],
  components: true,
})
